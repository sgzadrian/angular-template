import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout.routing';

/* This way all the shared components into ComponentsModule will be
 * available inside LayoutComponent
 **/
// import { ComponentsModule } from 'app/components/components.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    // ComponentsModule,
  ]
})
export class LayoutModule { }
