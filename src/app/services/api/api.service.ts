import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ApiService {

  base_url = environment.base_url;
  key = 'auth';

  token = null;
  loginType = 1;

  options = {
    headers: null,
  };

  private _auth = new BehaviorSubject<number>( 0 );

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
    this.resetHeaders();
    this.restoreAccess();
  }

  /* Login | Auth Functions */
  get auth(): Observable<number> { return this._auth; }

  hasAccess() { return !!this.token; }

  isAdmin() { return this.loginType === 0; }

  private resetHeaders() {
    this.options.headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  private setAuth( data: any ) {
    localStorage.setItem( this.key, JSON.stringify( data ) );
    this.token = data.token;
    this.options.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Token': data.token,
    });
    if ( data.type != undefined ) {
      this.loginType = data.type;
    }
    this._auth.next( 1 );
  }

  restoreAccess() {
    const auth = JSON.parse( localStorage.getItem( this.key ) );
    if ( auth && auth.token ) {
      this.setAuth( auth );
    }
  }

  login( data: any ) {
    this.http.post(
      this.base_url + '/login',
      data,
      this.options
    ).pipe(
    retry( 1 ),
    ).subscribe(( resp: any ) => {
      this.setAuth( resp );
    }, ( err ) => {
      console.error( 'Error ', err );
      this._auth.next( err.status == 0 ? 2 : err.status );
    });
  }

  logout() {
    if ( environment.production ) {
      localStorage.clear();
    }
    this.token = null;
    this.loginType = 1;
    this._auth.next( 0 );
    this.resetHeaders();
    this.router.navigateByUrl( '/login' );
  }

  private catchRequestError() {
    return catchError(( err: HttpErrorResponse ) => {
      if ( err.status === 401 ) {
        this.logout();
      } else {
        console.error( `Error: ${err.status}. Body was: ${err.message}` );
      }
      return throwError( err );
    });
  }

  /* Request Functions */
  fetch( endpoint: string ) {
    return this.http.get(
      this.base_url + endpoint,
      this.options
    ).pipe(
    retry(1),
    this.catchRequestError()
    );
  }

  get( endpoint: string, id: any ) {
    return this.http.get(
      this.base_url + endpoint + '/' + id,
      this.options
    ).pipe(
    retry(1),
    this.catchRequestError()
    );
  }

  post( endpoint: string, data = null ) {
    return this.http.post(
      this.base_url + endpoint,
      data,
      this.options
    ).pipe(
    retry(1),
    this.catchRequestError()
    );
  }

  patch( endpoint: string, id: any, data = null ) {
    return this.http.patch(
      this.base_url + endpoint + '/' + id,
      data,
      this.options
    ).pipe(
    retry(1),
    this.catchRequestError()
    );
  }

  delete( endpoint: string, id: any ) {
    return this.http.delete(
      this.base_url + endpoint + '/' + id,
      this.options
    ).pipe(
    retry(1),
    this.catchRequestError()
    );
  }

  batch( endpoint: string, data = null ) {
    return this.http.patch(
      this.base_url + endpoint + '/batch',
      { batch: data },
      this.options
    ).pipe(
    retry(1),
    this.catchRequestError()
    );
  }

}

