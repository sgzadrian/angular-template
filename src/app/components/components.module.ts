import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

// import { ExampleComponent } from './example/example.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
  ],
  declarations: [
    // ExampleComponent,
  ],
  exports: [
    // ExampleComponent,
  ]
})
export class ComponentsModule { }

